# Business Domain
Takeaway.com is an online food delivery marketplace connecting customers with restaurants. Our e-commerce platform allows customers
to choose from restaurants in their area and place orders from their menus. 
We have our own drivers that are responsible to deliver jobs to customers and
we use the term “job” to represent the delivery of the food from the restaurant to the customer.

# Problem
Build a service that will be responsible for assigning new jobs to currently working drivers. The service will continuously receive the following events:

- **JobCreatedEvent**  
This event indicates that a new order has been places and a new job has been created.
- **JobFetchedEvent**  
This event indicates that a driver just fetched a job from a restaurant.
- **JobDeliveredEvent**  
This event indicates that a driver just delivered a job to the customer.
- **DriverStartedWorkingEvent**  
This event indicates that a driver just started working and is available to handle jobs.
- **DriverStoppedWorkingEvent**  
This event indicates that a driver just stopped working and is not available to handle jobs any more.
- **DriverLocationChangedEvent**  
This event indicates that a driver just changed her/his location.

Implementation of these events in Java can be found [here](https://bitbucket.org/mylorrydevteam/job_assigner/src/master/src/main/java/com/takeaway/events/)

# Expected Solution
The service you build must be able to listen to these events and react to them.
The service should be responsible for finding a driver for each job. Once a driver for a job is found,
the service should produce a job assignment suggestion by emitting an *AssignmentSuggestedEvent*.
The implementation in Java for the desired *AssignmentSuggestedEvent* can be found [here](https://bitbucket.org/mylorrydevteam/job_assigner/src/master/src/main/java/com/takeaway/events/AssignmentSuggestedEvent.java)  
HINT: *AssignmentSuggestedEvent* can be emitted multiple times for the same job if the job is not yet fetched.

### Key Performance Indicator(KPI)
Each *JobCreatedEvent* contains a *scheduledFetchTime* which represents the time at which the restaurant will be ready with the food for a driver to fetch.
If a driver reaches a restaurant before the *scheduledFetchTime*, she/he should wait to fetch the food until the *scheduledFetchTime*.
A **fetch delay** is the duration between the *scheduledFetchTime* and the time when a driver fetches the food from the restaurant.
The KPI for the desired service is to keep these fetch delays minimal, otherwise the food quality becomes unacceptable and the restaurant needs to prepare it again.

# Notes
- You can create a simple listener by just implementing the [EventListener](https://bitbucket.org/mylorrydevteam/job_assigner/src/master/src/main/java/com/takeaway/events/EventListener.java) interface. Usage of message broker is not necessary.
- You can use the [EventEmitter](https://bitbucket.org/mylorrydevteam/job_assigner/src/master/src/main/java/com/takeaway/events/EventEmitter.java) class to emit *AssignmentSuggestedEvent*.
- You are allowed to use mock services in your implementation. An example of a mock service is a service that will mock driving duration calculations between two geo locations.