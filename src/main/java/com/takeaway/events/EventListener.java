package com.takeaway.events;

public interface EventListener {

    void handle(JobCreatedEvent jobCreatedEvent);

    void handle(JobFetchedEvent jobFetchedEvent);

    void handle(JobDeliveredEvent jobDeliveredEvent);

    void handle(DriverStartedWorkingEvent driverStartedWorkingEvent);

    void handle(DriverStoppedWorkingEvent driverStoppedWorkingEvent);

    void handle(DriverLocationChangedEvent driverLocationChangedEvent);

}
