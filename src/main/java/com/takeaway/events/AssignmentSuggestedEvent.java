package com.takeaway.events;

import java.time.Instant;

/**
 * This event should be emitted for every new job.
 */
public class AssignmentSuggestedEvent {

    /**
     * The ID of the driver, which is suggested as a good candidate for the new job
     */
    private final Long driverId;

    /**
     * The new job that need to be handle by a driver
     */
    private final Long jobId;

    /**
     * The expected time, when the driver will fetch the job from the restaurant
     */
    private final Instant expectedFetchTime;

    /**
     * The expected time, when the driver will deliver the job to the customer
     */
    private final Instant expectedDeliveryTime;

    public AssignmentSuggestedEvent(Long driverId, Long jobId, Instant expectedFetchTime, Instant expectedDeliveryTime) {
        this.driverId = driverId;
        this.jobId = jobId;
        this.expectedFetchTime = expectedFetchTime;
        this.expectedDeliveryTime = expectedDeliveryTime;
    }

    public Long getDriverId() {
        return driverId;
    }

    public Long getJobId() {
        return jobId;
    }

    public Instant getExpectedFetchTime() {
        return expectedFetchTime;
    }

    public Instant getExpectedDeliveryTime() {
        return expectedDeliveryTime;
    }

    @Override
    public String toString() {
        return "AssignmentSuggestedEvent{" +
                "driverId=" + driverId +
                ", jobId=" + jobId +
                ", expectedFetchTime=" + expectedFetchTime +
                ", expectedDeliveryTime=" + expectedDeliveryTime +
                '}';
    }

}