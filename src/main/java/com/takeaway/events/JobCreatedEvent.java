package com.takeaway.events;

import java.time.Instant;

/**
 * This event indicates that a new order has been places and a new delivery job has been created.
 */
public class JobCreatedEvent {

    /**
     * The ID of the job that has been created.
     */
    private final Long jobId;

    /**
     * Restaurant will be ready with cooking at this time and the driver could fetch the food.
     */
    private final Instant scheduledFetchTime;

    /**
     * The location of the restaurant
     */
    private final Location restaurantLocation;

    /**
     * The location of the customer
     */
    private final Location customerLocation;


    public JobCreatedEvent(Long jobId, Instant scheduledFetchTime, Location restaurantLocation, Location customerLocation) {
        this.jobId = jobId;
        this.scheduledFetchTime = scheduledFetchTime;
        this.restaurantLocation = restaurantLocation;
        this.customerLocation = customerLocation;
    }

    public Long getJobId() {
        return jobId;
    }

    public Instant getScheduledFetchTime() {
        return scheduledFetchTime;
    }

    public Location getRestaurantLocation() {
        return restaurantLocation;
    }

    public Location getCustomerLocation() {
        return customerLocation;
    }

}
