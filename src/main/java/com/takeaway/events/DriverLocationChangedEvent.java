package com.takeaway.events;

/**
 * This event indicates that a driver just changed her/his location.
 */
public class DriverLocationChangedEvent {

    /**
     * The ID of the driver
     */
    private final Long id;

    /**
     * New driver location
     */
    private final Location newLocation;

    public DriverLocationChangedEvent(Long id, Location newLocation) {
        this.id = id;
        this.newLocation = newLocation;
    }

    public Long getId() {
        return id;
    }

    public Location getNewLocation() {
        return newLocation;
    }

}
