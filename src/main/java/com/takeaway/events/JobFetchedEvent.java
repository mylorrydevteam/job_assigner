package com.takeaway.events;

import java.time.Instant;

/**
 * This event indicates that a driver just fetched a job from a restaurant.
 */
public class JobFetchedEvent {

    /**
     * The ID of the Job that just has been fetched
     */
    private final Long jobId;

    /**
     * The ID of the driver who just fetched the job
     */
    private final Long driverId;

    /**
     * The driver fetched the job at this time
     */
    private final Instant fetchedOn;

    public JobFetchedEvent(Long jobId, Long driverId, Instant fetchedOn) {
        this.jobId = jobId;
        this.driverId = driverId;
        this.fetchedOn = fetchedOn;
    }

    public Long getJobId() {
        return jobId;
    }

    public Long getDriverId() {
        return driverId;
    }

    public Instant getFetchedOn() {
        return fetchedOn;
    }

}
