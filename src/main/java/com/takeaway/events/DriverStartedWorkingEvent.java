package com.takeaway.events;

/**
 * This event indicates that a driver just started working and is available to handle jobs.
 */
public class DriverStartedWorkingEvent {

    /**
     * The ID of the driver that just started
     */
    private final Long id;

    public DriverStartedWorkingEvent(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}
