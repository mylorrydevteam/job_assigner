package com.takeaway.events;

/**
 * This event indicates that a driver just stopped working and is not available to handle jobs any more.
 */
public class DriverStoppedWorkingEvent {

    /**
     * The ID of the driver that just stopped
     */
    private final Long id;

    public DriverStoppedWorkingEvent(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}

