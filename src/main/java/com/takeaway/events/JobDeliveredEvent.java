package com.takeaway.events;

import java.time.Instant;

/**
 * This event indicates that a driver just delivered a job to the customer.
 */
public class JobDeliveredEvent {

    /**
     * The ID of the Job that just has been delivered
     */
    private final Long jobId;

    /**
     * The ID of the driver who just delivered the job
     */
    private final Long driverId;

    /**
     * The driver delivered the job at this time
     */
    private final Instant fetchedOn;

    public JobDeliveredEvent(Long jobId, Long driverId, Instant fetchedOn) {
        this.jobId = jobId;
        this.driverId = driverId;
        this.fetchedOn = fetchedOn;
    }

    public Long getJobId() {
        return jobId;
    }

    public Long getDriverId() {
        return driverId;
    }

    public Instant getFetchedOn() {
        return fetchedOn;
    }
}
